<?php

/**

 * The base configurations of the WordPress.

 *

 * This file has the following configurations: MySQL settings, Table Prefix,

 * Secret Keys, WordPress Language, and ABSPATH. You can find more information

 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing

 * wp-config.php} Codex page. You can get the MySQL settings from your web host.

 *

 * This file is used by the wp-config.php creation script during the

 * installation. You don't have to use the web site, you can just copy this file

 * to "wp-config.php" and fill in the values.

 *

 * @package WordPress

 */



// ** MySQL settings - You can get this info from your web host ** //

/** The name of the database for WordPress */

//define('WP_CACHE', true); //Added by WP-Cache Manager
define( 'WPCACHEHOME', '/home/azlogica/www/www/wp-content/plugins/wp-super-cache/' ); //Added by WP-Cache Manager

define('DB_NAME', 'azlogica_wpaz');



/** MySQL database username */

define('DB_USER', 'root');



/** MySQL database password */

define('DB_PASSWORD', '');



/** MySQL hostname */

define('DB_HOST', 'localhost');



/** Database Charset to use in creating database tables. */

define('DB_CHARSET', 'utf8');



/** The Database Collate type. Don't change this if in doubt. */

define('DB_COLLATE', '');



/**#@+

 * Authentication Unique Keys and Salts.

 *

 * Change these to different unique phrases!

 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}

 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.

 *

 * @since 2.6.0

 */

define('AUTH_KEY',         'a8dde40on6tqpmdjrpgykygqiholjsy6g3z0mkilzxcwxic1i4fhw64gf7lnwni9');

define('SECURE_AUTH_KEY',  'ewa7wmwagltfxlgskjsxf1lfklkhh0qgbn04bi3utcgbioxcdxgivcqqo9opn6qg');

define('LOGGED_IN_KEY',    'dyuuedpvtzmoplvbzi9p7ebdulqmxef7ijlccd8vgstpiiezmqkn2afnylclaakk');

define('NONCE_KEY',        'zvayoxy9h2fjyzf4ch5mqk0axu96u2bklu5je0uwzfrwhjsauuyna3avwl82jdip');

define('AUTH_SALT',        'el56uoutyipbdvp5k4sasqh7dffgycv5b8wbh8epma5psqzag2lsr9pb9o5slgtk');

define('SECURE_AUTH_SALT', 'qgk4bcogymvug7jzj0efgvdxkbmvo5m5cytgrrhanz1bpfzfbbwsqomvdli0ix3m');

define('LOGGED_IN_SALT',   'kldjcy7q0rn1y3kxit9ds9qj0yhlig8h7txqycpw5pjyq6t30yqgitxxkg85hnnr');

define('NONCE_SALT',       'kjdptepyznlnllj6vdkljg8q0sleodq3fcxtjlbeda3rvn6ebfjcwrsxs699tblb');



/**#@-*/



/**

 * WordPress Database Table prefix.

 *

 * You can have multiple installations in one database if you give each a unique

 * prefix. Only numbers, letters, and underscores please!

 */

$table_prefix  = '9b8_';



/**

 * WordPress Localized Language, defaults to English.

 *

 * Change this to localize WordPress. A corresponding MO file for the chosen

 * language must be installed to wp-content/languages. For example, install

 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German

 * language support.

 */

define('WPLANG', 'es_ES');



/**

 * For developers: WordPress debugging mode.

 *

 * Change this to true to enable the display of notices during development.

 * It is strongly recommended that plugin and theme developers use WP_DEBUG

 * in their development environments.

 */

define('WP_DEBUG', false);



/* That's all, stop editing! Happy blogging. */



/** Absolute path to the WordPress directory. */

if ( !defined('ABSPATH') )

	define('ABSPATH', dirname(__FILE__) . '/');



/** Sets up WordPress vars and included files. */

require_once(ABSPATH . 'wp-settings.php');

