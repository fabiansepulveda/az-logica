<?php get_header(); 
?>
<div style="width:100%; height:833px; position:relative; z-index: 1">
		<div id="map" style="width:100%; height:833px">
		</div>
		<div id="contact_form" style="top: 130px;">
			<?php 
			if (ICL_LANGUAGE_CODE == 'en') {?>
			<?php echo do_shortcode( '[contact-form-7 id="623" title="Formulario de Contacto_en"]' ) ?>
			<?php } else {?>
			<?php echo do_shortcode( '[contact-form-7 id="4" title="Formulario de Contacto"]' ) ?>
			<?php }
			?>
		</div>
		
			<div id="sedes" style="width:100%; height:103px; background-color: #4c3e3b; position: absolute; bottom: 0px; opacity: 0.8; overflow: auto;">
				<div class="nuestrassedes">
				<p><?php _e('Our','azlogica'); ?> <span> <?php _e('Locations','azlogica'); ?> </span></p>
				<?php
				$args = array(
				    'orderby' => 'date', 
					'order' => 'ASC',
				    );
				$custom_terms = get_terms('paises', $args);
				echo '<ul id="list_sedes">';
								foreach($custom_terms as $custom_term) {
									wp_reset_query();
									$args = array(
										'orderby' => 'date', 
										'order' => 'DESC',
										'post_type' => 'sedes',
										'tax_query' => array(
											array(
												'orderby' => 'date', 
												'order' => 'DESC',
												'taxonomy' => 'paises',
												'field' => 'slug',
												'terms' => $custom_term->slug,
												
												
											),
										),
									 );
								
									 $loop = new WP_Query($args);
									 
									 if($loop->have_posts()) {
										 $sinespacio = str_replace(' ', '_', $custom_term->name);
										 $sinespacio = strtolower($sinespacio);
										 $sinespacio =removeAccents ($sinespacio);
										 echo '<li id="mapa_'.$sinespacio.'" class="ciudad">'.$custom_term->name.'</li>';
										/*
										echo '<div id="sedes_'.$sinespacio.'" class="sedes">';	
										
										echo '<ul>';				
										while($loop->have_posts()) : $loop->the_post();
											$ciudadespacio = str_replace(' ', '_', get_the_title());
											$ciudadespacio = strtolower($ciudadespacio);
											echo '<li id="sede_'.$ciudadespacio.'">'.get_the_title().'</li>';
										endwhile;
										echo '<ul>';
										
										//Fin Sedes
										echo '</div>';*/
										//Fin de la ciudad
										
									 }
									
								}
								 echo "</ul>";
				?>
				</div>
		<div id="representantes"><p><?php _e('Agents and dealers in','azlogica'); ?> <span> <?php _e('Latin America','azlogica'); ?> </span></p></div>
		</div>
		</div>
<?php get_footer(); ?>