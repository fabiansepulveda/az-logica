<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<div class="space-header"></div>
<div id="related-news">
        <?php if ( have_posts() ) :
			while ( have_posts() ) : the_post(); 
				$subtitulo = get_post_meta( $post->ID, 'my_meta_box_text', true );

				echo '<div class="box_type_3">';
				?>
				<div class="box_text">
					<span class="date"><?php the_time('M. j. Y'); ?></span>
					<?php the_title( '<h2>', '</h2>' ); 
					echo '<p>' . substr(get_the_excerpt(), 0,140) . '</p>'; ?>
				</div>
					<?php echo '<a class="more" href="' . get_permalink(). '""></a>';?>
				</div>

			<?php endwhile; ?>
               <?php else : ?>
               <div class="not_found">
                        <h2 class="title_search_result"><?php _e( 'No se han encontrado resultados para tu búsqueda.', 'azlogica' ); ?></h2>
						<p class="notFound"><?php _e( 'Sugerencia:<ul><li>Aségurate de que todas las palabras estén escritas correctamente.</li><li>Prueba diferentes palabras claves.</li><li>Prueba palabras claves más grandes</li>', 'azlogica' ); ?></p>
				</div>
                <?php endif; ?>
</div>
<?php get_footer(); ?>