<?php
//Add Meta Boxes

/**
 * CMB Theme Options
 * @version 0.1.0
 */
class myprefix_Admin {

    /**
     * Option key, and option page slug
     * @var string
     */
    private $key = 'site_options';

    /**
     * Array of metaboxes/fields
     * @var array
     */
    protected $option_metabox = array();

    /**
     * Options Page title
     * @var string
     */
    protected $title = '';

    /**
     * Options Page hook
     * @var string
     */
    protected $options_page = '';

    /**
     * Constructor
     * @since 0.1.0
     */
    public function __construct() {
        // Set our title
        $this->title = __( 'Opciones del sitio', 'myprefix' );
    }

    /**
     * Initiate our hooks
     * @since 0.1.0
     */
    public function hooks() {
        add_action( 'admin_init', array( $this, 'init' ) );
        add_action( 'admin_menu', array( $this, 'add_options_page' ) );
    }

    /**
     * Register our setting to WP
     * @since  0.1.0
     */
    public function init() {
        register_setting( $this->key, $this->key );
    }

    /**
     * Add menu options page
     * @since 0.1.0
     */
    public function add_options_page() {
        $this->options_page = add_menu_page( $this->title, $this->title, 'manage_options', $this->key, array( $this, 'admin_page_display' ) );
    }

    /**
     * Admin page markup. Mostly handled by CMB
     * @since  0.1.0
     */
    public function admin_page_display() {
        ?>
        <div class="wrap cmb_options_page <?php echo $this->key; ?>">
            <h2><?php echo esc_html( get_admin_page_title() ); ?></h2>
            <?php cmb_metabox_form( self::option_fields(), $this->key ); ?>
        </div>
        <?php
    }

    /**
     * Defines the theme option metabox and field configuration
     * @since  0.1.0
     * @return array
     */
    public function option_fields() {

        // Only need to initiate the array once per page-load
        if ( ! empty( $this->option_metabox ) ) {
            return $this->option_metabox;
        }
        $link =  get_bloginfo ('wpurl');
        $this->fields = array(
            array(
                'name' => 'Favicon',
                'desc' => 'Imagen de 16px x 16px (.ico) que represente su sitio web.',
                'id' => 'favicon_image',
                //'default' => '',
                'type' => 'file'
            ),
            array(
                'name' => 'Logo',
                'id' => 'logo',
                'type' => 'file',
                'default' => $link . '/wp-content/uploads/logo.png'
            ),
            array(
                'name' => 'Apple Touch Icon 57x57',
                'id' => 'apple_icon_57',
                'type' => 'file'
            ),
            array(
                'name' => 'Apple Touch Icon 72x72',
                'id' => 'apple_icon_72',
                'type' => 'file'
            ),
            array(
                'name' => 'Apple Touch Icon 114x114',
                'id' => 'apple_icon_114',
                'type' => 'file'
            ),
            array(
                'name' => 'Apple Touch Icon 144x144',
                'id' => 'apple_icon_144',
                'type' => 'file'
            ),
            array(
                'name' => 'Información acerca de AZ Lógica',
                'id' => 'about_company',
                'type' => 'textarea'
            ),
            array(
                'name' => 'Información acerca de AZ Lógica-EN',
                'id' => 'about_company_en',
                'type' => 'textarea'
            ),
            array(
                'name' => 'Información Oficina Principal',
                'id' => 'info_office',
                'type' => 'textarea'
            ),
            array(
                'name'    => __( 'Noticia Destacada', 'cmb' ),
                'desc'    => __( 'Seleccione la noticia destaca ', 'cmb' ),
                'id'      => 'select_noticia',
                'type'    => 'select',
                'options' => cmb_get_post_options( array( 'post_type' => 'post', 'numberposts' => -1, 'cat' => 10 ) ),
            ),
            array(
                'name'    => __( 'Blog Destacado', 'cmb' ),
                'desc'    => __( 'Seleccione el articulo destacado para el blog', 'cmb' ),
                'id'      => 'select_blog',
                'type'    => 'select',
                'options' => cmb_get_post_options( array( 'post_type' => 'post', 'numberposts' => -1, 'cat' => 11 ) ),
            ),
        );

        $this->option_metabox = array(
            'id'         => 'option_metabox',
            'show_on'    => array( 'key' => 'options-page', 'value' => array( $this->key, ), ),
            'show_names' => true,
            'fields'     => $this->fields,
        );

        return $this->option_metabox;


    }

    /**
     * Public getter method for retrieving protected/private variables
     * @since  0.1.0
     * @param  string  $field Field to retrieve
     * @return mixed          Field value or exception is thrown
     */
    public function __get( $field ) {

        // Allowed fields to retrieve
        if ( in_array( $field, array( 'key', 'fields', 'title', 'options_page' ), true ) ) {
            return $this->{$field};
        }
        if ( 'option_metabox' === $field ) {
            return $this->option_fields();
        }

        throw new Exception( 'Invalid property: ' . $field );
    }

}

// Get it started
$myprefix_Admin = new myprefix_Admin();
$myprefix_Admin->hooks();

/**
 * Wrapper function around cmb_get_option
 * @since  0.1.0
 * @param  string  $key Options array key
 * @return mixed        Option value
 */
function myprefix_get_option( $key = '' ) {
    global $myprefix_Admin;
    return cmb_get_option( $myprefix_Admin->key, $key );
}
/**
 * Gets a number of posts and displays them as options
 * @param  array $query_args Optional. Overrides defaults.
 * @return array             An array of options that matches the CMB options array
 */
function cmb_get_post_options( $query_args ) {

    $args = wp_parse_args( $query_args, array(
        'post_type' => 'post',
        'numberposts' => 10,
    ) );

    $posts = get_posts( $args );

    $post_options = array();
    if ( $posts ) {
        foreach ( $posts as $post ) {
                   $post_options[] = array(
                       'name' => $post->post_title,
                       'value' => $post->ID
                   );
        }
    }

    return $post_options;
}