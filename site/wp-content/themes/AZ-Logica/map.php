<!-- Mapa-->

<script type="text/javascript">
	var map;        
	var myCenter=new google.maps.LatLng(4.6433726236618424, -74.09134575561522);
	var pin = new google.maps.MarkerImage
	(	'<?php bloginfo('template_directory'); ?>/assets/map/pin.png',
		new google.maps.Size(26,55),
		new google.maps.Point(0,0),
		new google.maps.Point(13,55)
		);
		
	<?php  
						
						$custom_terms = get_terms('paises');
						foreach($custom_terms as $custom_term) {
							wp_reset_query();
							$args = array('post_type' => 'sedes',
								'tax_query' => array(
									array(
										'taxonomy' => 'paises',
										'field' => 'slug',
										'terms' => $custom_term->slug,
									),
								),
							 );
						
							 $loop = new WP_Query($args);
							 if($loop->have_posts()) {
								 $sinespacio = str_replace(' ', '_', $custom_term->name);
								 $sinespacio = strtolower($sinespacio);
								echo "//Pais ".	$custom_term->name."\n";
									
								while($loop->have_posts()) : $loop->the_post();
									$ciudadespacio = str_replace(' ', '_', get_the_title());
									$pattern = '/[^a-zA-Z0-9]/';
									$ciudadespacio= preg_replace($pattern, '',$ciudadespacio);
									$ciudadespacio = strtolower($ciudadespacio);
							
									$latitud= wpshed_get_custom_field( "wpshed_textfield_lat");
									$longitud = wpshed_get_custom_field( "wpshed_textfield_long");
									$sedenom  = "sede_".$ciudadespacio."_pos";
									echo "var ".$sedenom." = new google.maps.LatLng(".$latitud.", ".$longitud.");\n";
									echo 'var marker_'.$ciudadespacio.' = new google.maps.Marker({
										position: '.$sedenom.',
										map: map,
										icon: pin,
										title:"'.get_the_title().'"
										});'
										;
								endwhile;
								
							 }
						}
						?>			
	

function initialize() {
  
  var myOptions = {
    zoom: 15,
	scrollwheel: false,
	panControl: true,
    panControlOptions: {
        position: google.maps.ControlPosition.LEFT_CENTER
    },
	zoomControl: true,
    zoomControlOptions: {
        style: google.maps.ZoomControlStyle.LARGE,
        position: google.maps.ControlPosition.LEFT_CENTER
    },
	center:myCenter,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    styles: [
    {
			"stylers": [
				{
					"hue": "#fab0a4"
				}
			]
		},
		{
			"featureType": "road",
			"elementType": "labels",
			"stylers": [
				{
					"visibility": "off"
				}
			]
		},
		{
			"featureType": "road",
			"elementType": "geometry",
			"stylers": [
				{
					"lightness": 100
				},
				{
					"visibility": "simplified"
				}
			]
		}
	]
};
  
  
  map=new google.maps.Map(document.getElementById("map"),myOptions);
  <?php
  /*
  $custom_terms = get_terms('paises');
						foreach($custom_terms as $custom_term) {
							wp_reset_query();
							$args = array('post_type' => 'sedes',
								'tax_query' => array(
									array(
										'taxonomy' => 'paises',
										'field' => 'slug',
										'terms' => $custom_term->slug,
										
									),
								),
							 );
						
							 $loop = new WP_Query($args);
							 if($loop->have_posts()) {
								 $sinespacio = str_replace(' ', '_', $custom_term->name);
								 $sinespacio = strtolower($sinespacio);
								 $url_pag = get_bloginfo('stylesheet_directory');
												
								while($loop->have_posts()) : $loop->the_post();
									$ciudadespacio = str_replace(' ', '_', get_the_title());
									$pattern = '/[^a-zA-Z0-9]/';
									$ciudadespacio= preg_replace($pattern, '',$ciudadespacio);
									$ciudadespacio = strtolower($ciudadespacio);
									$direccion= wpshed_get_custom_field( "wpshed_textfield_direccion");
									$telefono = wpshed_get_custom_field( "wpshed_textfield_telefono");
									$email = wpshed_get_custom_field( "wpshed_textfield_email");
									echo 'marker_'.$ciudadespacio.'.setMap(map);
										var boxText_'.$ciudadespacio.' = document.createElement("div");
										boxText_'.$ciudadespacio.'.style.cssText = "margin-top:-160px;  background: #193d53; padding: 15px;";
										boxText_'.$ciudadespacio.'.id="info_mapa";
										boxText_'.$ciudadespacio.'.innerHTML = "<p>'.get_the_title().'</p><p><span>DIRECCI�N:</span>'.$direccion.'</p><p><span>TEL�FONO:</span>'.$telefono.'</p><p><span>EMAIL:</span>'.$email.'</p>";
									var myOptions_'.$ciudadespacio.' = {
										 content: boxText_'.$ciudadespacio.'
										,disableAutoPan: false
										,maxWidth: 0
										,pixelOffset: new google.maps.Size(-140, 0)
										,zIndex: null
										,boxStyle: { 
											width: "280px"
										 }
										,closeBoxMargin: "-155px 4px 2px 2px"
										,closeBoxURL: "'.$url_pag.'/assets/map/closeButton.png"
										,infoBoxClearance: new google.maps.Size(1, 1)
										,isHidden: false
										,pane: "floatPane"
										,enableEventPropagation: false
									};
									
								
									var ib_'.$ciudadespacio.' = new InfoBox(myOptions_'.$ciudadespacio.');
                                                                        window.ib_'.$ciudadespacio.' = ib_'.$ciudadespacio.';
									google.maps.event.addListener(marker_'.$ciudadespacio.', "click", function(){
										
										 ib_'.$ciudadespacio.'.open(map, marker_'.$ciudadespacio.');
										});
										';
								endwhile;
								
							 }
						}
  
  ?>

google.maps.event.addDomListener(window, 'load', initialize);

google.maps.event.addDomListener(window, "resize", resizingMap());

   <?php  
						
	$custom_terms = get_terms('paises');
	foreach($custom_terms as $custom_term) {
		wp_reset_query();
		$args = array('post_type' => 'sedes',
			'tax_query' => array(
				array(
					'taxonomy' => 'paises',
					'field' => 'slug',
					'terms' => $custom_term->slug,
				),
			),
		 );
	
		 $loop = new WP_Query($args);
		 if($loop->have_posts()) {
			 $sinespacio = str_replace(' ', '_', $custom_term->name);
			 $sinespacio = strtolower($sinespacio);				
			while($loop->have_posts()) : $loop->the_post();
				$latitud= wpshed_get_custom_field( "wpshed_textfield_lat");
				$longitud = wpshed_get_custom_field( "wpshed_textfield_long");
				break;
			endwhile;
			echo '
				$( "#mapa_'.$sinespacio.' p" ).click(function cambiarCentro() {
			   if(typeof map =="undefined") return;
			   var center = new google.maps.LatLng('.$latitud.', '.$longitud.');
			   google.maps.event.trigger(map, "resize");
			   map.setCenter(center); 
			   map.setZoom(11);
			   //$( "#sedes_'.$sinespacio.'" ).toggle("slow");
			});';
		 }
	}
	foreach($custom_terms as $custom_term) {
		wp_reset_query();
		$args = array('post_type' => 'sedes',
			'tax_query' => array(
				array(
					'taxonomy' => 'paises',
					'field' => 'slug',
					'terms' => $custom_term->slug,
				),
			),
		 );
	
		 $loop = new WP_Query($args);
		 if($loop->have_posts()) {
			 $sinespacio = str_replace(' ', '_', $custom_term->name);
			 $sinespacio = strtolower($sinespacio);				
			while($loop->have_posts()) : $loop->the_post();
				$latitud= wpshed_get_custom_field( "wpshed_textfield_lat");
				$longitud = wpshed_get_custom_field( "wpshed_textfield_long");
				$ciudadespacio = str_replace(' ', '_', get_the_title());
				$pattern = '/[^a-zA-Z0-9]/';
				$ciudadespacio= preg_replace($pattern, '',$ciudadespacio);
				$ciudadespacio = strtolower($ciudadespacio);
				$descripcion = get_the_content();
				echo '$( "#sede_'.$ciudadespacio.'" ).click(function cambiarCentro() {
					   if(typeof map =="undefined") return;
					   var center = new google.maps.LatLng('.$latitud.', '.$longitud.');
					   google.maps.event.trigger(map, "resize");
					   map.setCenter(center); 
					   map.setZoom(16);
                                           ib_'.$ciudadespacio.'.open(map, marker_'.$ciudadespacio.');
					   var spanito=document.getElementById("descripcion_sede");
					   var nombresed=document.getElementById("nombre_sede");
					 
					   
					   
					});';
			endwhile;
		 }
	}		
		*/?>	


//});

function resizeMap() {
   if(typeof map =="undefined") return;
   setTimeout( function(){resizingMap();} , 400);
}

function resizingMap() {
   if(typeof map =="undefined") return;
   var center = map.getCenter();
   google.maps.event.trigger(map, "resize");
   map.setCenter(center); 
}



</script>
<!--Fin del Mapa-->