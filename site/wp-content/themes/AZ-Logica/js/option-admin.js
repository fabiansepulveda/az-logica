jQuery(function($)
    {
    function my_check_categories() {
        $('#metabox_link').hide();
        $('#categorychecklist input[type="checkbox"]').each(function(i,e)
        {
        var id = $(this).attr('id').match(/-([0-9]*)$/i);
        id = (id && id[1]) ? parseInt(id[1]) : null ;
        if ($.inArray(id, [9]) > -1 && $(this).is(':checked'))
        {
        $('#metabox_link').show();
        }
        });
    }
    $('#categorychecklist input[type="checkbox"]').live('click', my_check_categories);
    my_check_categories();
});