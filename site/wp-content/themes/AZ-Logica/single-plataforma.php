<?php get_header(); ?>

<div id="fondo">
	<?php the_post_thumbnail('full'); ?>
	<div class="container tituloInterna">
		<div class="row col-lg-offset-1">
			<h1>
				<?php echo get_the_title( $post_id ); ?>
			</h1>
		</div>
	</div>
</div>

<div id="paginaPlataforma">

	<div class="container">

		<div class="row breadcrumbs">
			<?php
				if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10">','</div>');
				}
			?>
		</div>
		
		<div class="row col-lg-offset-1">

			<div class="col-sm-7 col-xs-10" id="contPost">
				<h2 class="subtitulo">
					<?php echo get_the_title( $post_id ); ?>
				</h2>
				<?php the_content(); ?>
			</div>


			<div class="col-sm-3 col-xs-10" id="sidebar">

				<h2 class="sidebar">
					<?php _e('<span>Learn more about</span> 
					<br>our platform','azlogica');?>
				</h2>

				<?php $related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => 3, 'post__not_in' => array($post->ID) ) );
	      		  if( $related ) foreach( $related as $post ) {
	      		  setup_postdata($post); ?>

		            <div class="plataforma">
						<a href="<?php the_permalink() ?>">
							<?php $image_auxiliar = wp_get_attachment_image( get_post_meta( get_the_ID(), '_wpb_img_aux_id', 1 ), 'full' );
							echo $image_auxiliar;
							 //the_post_thumbnail( 'single-post-thumbnail' ); ?>
							<h2>
								<?php the_title(); ?>
							</h2>
							<span class="hover_info">
								<h2>
									<?php the_title(); ?>
								</h2>
							</spam>
						</a>
					</div>

	            	<?php } wp_reset_postdata(); ?>

			</div>
		</div>
	</div>
</div>


<?php get_footer(); ?>