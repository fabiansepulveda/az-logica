// Sass configuration
var gulp = require("gulp");
var sass = require('gulp-sass');
var gutil = require("gulp-util");
var concat = require("gulp-concat");
var mainBowerFiles = require("main-bower-files");
var uglify = require("gulp-uglify");
var streamqueue = require('streamqueue');
var nano = require('gulp-cssnano');
var less = require("gulp-less");
var minifyCss = require('gulp-minify-css');
var rename = require("gulp-rename");

var debug = true;
var outputRoot = "";

gulp.task('appcss', function() {
    gulp.src('devcss/*.css')
        .pipe(rename({suffix: ".min"}))
        .pipe(minifyCss({compatibility: 'ie8'}))
        .pipe(gulp.dest("css/"))
});

gulp.task("default",function() {
    gulp.watch('devcss/*.css', ['appcss']);
} );

gulp.task("debug", function () {
    debug = true;
    gutil.log(gutil.colors.green('RUNNING IN DEBUG MODE'));
    gulp.start('default');
})