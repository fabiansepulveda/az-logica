<div id="footer">
	<div class="row limpia" id="info">
		<div class="col-sm-3 col-xs-12  coninfo">
			<h3>
				<span><?php _e('About','azlogica');?></span> <?php _e('Azlogica','azlogica');?>
			</h3>
			<p>
				<?php if (ICL_LANGUAGE_CODE == 'en') {
					echo myprefix_get_option( 'about_company_en' );
				} else {
					echo myprefix_get_option( 'about_company' ); 
				}?>
			</p>
		</div>
		<div class="col-xs-12 col-sm-3 coninfo">
			<h3>
				<span><?php _e('azlogica','azlogica');?></span> <?php _e('on twitter','azlogica');?>
			</h3>
			<?php if ( is_active_sidebar( 'sidebar-twitter' ) ) : ?>
				<ul id="sidebar">
					<?php dynamic_sidebar( 'sidebar-twitter' ); ?>
				</ul>
			<?php endif; ?>
		</div>
		<div class="col-xs-12 col-sm-3 coninfo">
			<h3>
				<?php _e('<span>Useful</span> Tags','azlogica');?>
			</h3>		
			<?php wp_tag_cloud( array('number'=>10, 'orderby'=>'count', 'order'=>'DESC', 'format'=>'list') );?>
		</div>
		<div class="col-xs-12 col-sm-3 coninfo">
			<h3>
				<?php _e('<span>Main</span> Office','azlogica');?>
			</h3>
			<p>
				<?php echo myprefix_get_option( 'info_office' ); ?>
			</p>
		</div>
	</div>
	<div class="row limpia" id="contacto">
		<div class="col-sm-3 col-xs-12 concon izq">
			<p>
				<?php _e('Designed and developed by','azlogica');?> <a href="http://dynamikcollective.com/" target="_blank">dynamikcollective.com</a>
			<p>
		</div>
		<div class="col-sm-3 col-xs-12 concon cent">
			<p>
				<?php _e('Azlogica ® 2014, All rights reserved.','azlogica');?>
			</p>
		</div>
		<div class="col-sm-3 col-xs-12 concon der">
			<div class="wrapper_news">
				<?php if (ICL_LANGUAGE_CODE == 'en') {
					echo do_shortcode('[contact-form-7 id="629" title="newsletter_en" html_class="use-floating-validation-tip"]');
				} else {
					echo do_shortcode('[contact-form-7 id="439" title="newsletter" html_class="use-floating-validation-tip"]');
				}?>
			</div>
		</div>
		<div class="col-sm-3 col-xs-12 concon der">
			<ul>
				<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Sidebar redes sociales") ) : ?>  
				<?php endif; ?>
			</ul>
		</div>
	</div>
</div>
<!--mapa-->
<?php get_template_part( 'map2' ); ?> 
<!--mapa-->
<script src="<?php bloginfo('stylesheet_directory'); ?>/js/footer-scripts.js"></script>

<?php

if(!is_home()) { ?>
	<script src="<?php bloginfo('stylesheet_directory'); ?>/js/footer-nothome-scripts.js"></script>

<?php } ?>

</script>
<?php wp_footer(); ?>
</body>

</html>