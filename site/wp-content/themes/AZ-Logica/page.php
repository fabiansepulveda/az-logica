<?php get_header(); ?>
<div id="paginaEmpresa">
	<div class="container">
		<div class="row breadcrumbs">
			<?php
				if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10">','</div>');
				}
			?>
		</div>
		<div class="row col-lg-offset-1">
			<div class="col-sm-3 col-xs-10" id="menuEmpresa">
				<ul>
					<?php
				  if($post->post_parent)
				  $children = wp_list_pages("title_li=&child_of=".$post->post_parent."&echo=0");
				  else
				  $children = wp_list_pages("title_li=&child_of=".$post->ID."&echo=0");
				  if ($children) {
				$parent_title = get_the_title($post->post_parent);?>
				<li><a href="<?php echo get_permalink($post->post_parent) ?>"><?php echo $parent_title;?></a></li>
				  <?php echo $children; ?>
				  <?php } ?>
				</ul>
			</div>
		
			<div class="col-sm-7 col-xs-10" id="contPost">
				<h2 class="subtitulo">
					<?php echo the_title();?>
				</h2>
				<p><?php echo get_the_content();?></p>
			</div>
		</div>

	</div>
</div>

<?php get_footer(); ?>