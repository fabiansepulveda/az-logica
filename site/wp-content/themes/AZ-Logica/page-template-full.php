<?php
/**
 * Template Name: Full Width Page
 *
 */
 ?>
 <?php get_header(); ?>
<div class="wrapper-page-full container">
	<div class="row breadcrumbs">
		<?php
			if ( function_exists('yoast_breadcrumb') ) {
				yoast_breadcrumb('<div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10">','</div>');
			}
		?>
	</div>	
	<div class="col-xs-12 col-sm-12 col-md-offset-2 col-md-8 content-info" id="contPost">
		<?php the_title('<h1>','</h1>'); ?>
		<?php the_content(); ?>
	</div>
</div>

<?php get_footer(); ?>