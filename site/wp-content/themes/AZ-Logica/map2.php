<script>
	var map;        
	var myCenter=new google.maps.LatLng(4.6433726236618424, -74.09134575561522);
	var pin = new google.maps.MarkerImage
	(	'<?php bloginfo('template_directory'); ?>/assets/map/pin.png',
		new google.maps.Size(13,13),
		new google.maps.Point(0,0),
		new google.maps.Point(6,13)
		);
		
	<?php  
						
			$custom_terms = get_terms('paises');
			foreach($custom_terms as $custom_term) {
				wp_reset_query();
				$args = array('post_type' => 'sedes',
					'tax_query' => array(
						array(
							'taxonomy' => 'paises',
							'field' => 'slug',
							'terms' => $custom_term->slug,
						),
					),
				 );
			
				 $loop = new WP_Query($args);
				 if($loop->have_posts()) {
					 $sinespacio = str_replace('-', '_', $custom_term->slug);
					 $sinespacio = strtolower($sinespacio);
					 $sinespacio = removeAccents($sinespacio);
					echo "//Pais ".	$custom_term->slug."\n";
					$agregado = $sinespacio;
						
					while($loop->have_posts()) : $loop->the_post();
						$ciudadespacio = str_replace('-', '_', get_the_title());
						$pattern = '/[^a-zA-Z0-9]/';
						$ciudadespacio= preg_replace($pattern, '',$ciudadespacio);
						$ciudadespacio = strtolower($ciudadespacio);
						$ciudadespacio = $ciudadespacio.$agregado;
				
						$latitud= wpshed_get_custom_field( "wpshed_textfield_lat");
						$longitud = wpshed_get_custom_field( "wpshed_textfield_long");
						$sedenom  = "sede_".$ciudadespacio."_pos";
						echo "var ".$sedenom." = new google.maps.LatLng(".$latitud.", ".$longitud.");\n";
						echo 'var marker_'.$ciudadespacio.' = new google.maps.Marker({
							position: '.$sedenom.',
							map: map,
							icon: pin,
							title:"'.get_the_title().'"
							});'
							;
					endwhile;
					
				 }
			}
?>		



var myOptions = {
    zoom: 11,
	scrollwheel: false,
	streetViewControl: false,
	panControl: true,
    panControlOptions: {
        position: google.maps.ControlPosition.LEFT_CENTER
    },
	zoomControl: true,
    zoomControlOptions: {
        style: google.maps.ZoomControlStyle.LARGE,
        position: google.maps.ControlPosition.LEFT_CENTER
    },
    center: myCenter,
    mapTypeId: google.maps.MapTypeId.ROADMAP,
    styles: [
    {
			"stylers": [
				{
					"hue": "#fab0a4"
				}
			]
		},
		{
			"featureType": "road",
			"elementType": "labels",
			"stylers": [
				{
					"visibility": "off"
				}
			]
		},
		{
			"featureType": "road",
			"elementType": "geometry",
			"stylers": [
				{
					"lightness": 100
				},
				{
					"visibility": "simplified"
				}
			]
		}
	]
};

var map = new google.maps.Map(document.getElementById('map'), myOptions);

 var opt = { minZoom: 6, maxZoom: 16 };
 map.setOptions(opt);


//google.maps.event.addDomListener(window, "resize", resizingMap());


jQuery(document).ready(function($) {
   <?php  
						
	$custom_terms = get_terms('paises');
	foreach($custom_terms as $custom_term) {
		wp_reset_query();
		$args = array('post_type' => 'sedes',
			'tax_query' => array(
				array(
					'taxonomy' => 'paises',
					'field' => 'slug',
					'terms' => $custom_term->slug,
				),
			),
		 );
	
		 $loop = new WP_Query($args);
		 if($loop->have_posts()) {
			 $sinespacio = str_replace('-', '_', $custom_term->slug);
			 $sinespacio = strtolower($sinespacio);	
			  $sinespacio = removeAccents($sinespacio);
             $agregado = $sinespacio;			 
				
			while($loop->have_posts()) : $loop->the_post();
				$latitud= wpshed_get_custom_field( "wpshed_textfield_lat");
				$longitud = wpshed_get_custom_field( "wpshed_textfield_long");
				$ciudadespacio = str_replace('-', '_', get_the_title());
				$pattern = '/[^a-zA-Z0-9]/';
				$ciudadespacio= preg_replace($pattern, '',$ciudadespacio);
				$ciudadespacio = strtolower($ciudadespacio);
				$ciudadespacio = $ciudadespacio.$agregado;
				break;
			endwhile;
			echo '
				$( "#mapa_'.$agregado.'" ).click(function cambiarCentro() {
			   if(typeof map =="undefined") return;
			   var center = new google.maps.LatLng('.$latitud.', '.$longitud.');
			   google.maps.event.trigger(map, "resize");
			   map.setCenter(center);' ;
			   
               if ($agregado == "colombia"){
                   echo 'map.setZoom(7);';
               }
               else{
                   echo 'map.setZoom(16);';
               }
			   echo 'ib_'.$ciudadespacio.'.open(map, marker_'.$ciudadespacio.');
			   //$( "#sedes_'.$sinespacio.'" ).toggle("slow");
			});';
		 }
	}
	/*
	foreach($custom_terms as $custom_term) {
		wp_reset_query();
		$args = array('post_type' => 'sedes',
			'tax_query' => array(
				array(
					'taxonomy' => 'ciudades',
					'field' => 'slug',
					'terms' => $custom_term->slug,
				),
			),
		 );
	
		 $loop = new WP_Query($args);
		 if($loop->have_posts()) {
			 $sinespacio = str_replace(' ', '_', $custom_term->slug);
			 $sinespacio = strtolower($sinespacio);				
			while($loop->have_posts()) : $loop->the_post();
				$latitud= wpshed_get_custom_field( "wpshed_textfield_lat");
				$longitud = wpshed_get_custom_field( "wpshed_textfield_long");
				$ciudadespacio = str_replace(' ', '_', get_the_title());
				$pattern = '/[^a-zA-Z0-9]/';
				$ciudadespacio= preg_replace($pattern, '',$ciudadespacio);
				$ciudadespacio = strtolower($ciudadespacio);
				$descripcion = get_the_content();
				echo '$( "#sede_'.$ciudadespacio.'" ).click(function cambiarCentro() {
					   if(typeof map =="undefined") return;
					   var center = new google.maps.LatLng('.$latitud.', '.$longitud.');
					   google.maps.event.trigger(map, "resize");
					   map.setCenter(center); 
					   map.setZoom(16);
                                           ib_'.$ciudadespacio.'.open(map, marker_'.$ciudadespacio.');
					   var spanito=document.getElementById("descripcion_sede");
					   var nombresed=document.getElementById("nombre_sede");
					 
					   
					   
					});';
			endwhile;
		 }
	}	*/	
		?>	
 });
 
 
 
 
<?php
		$custom_terms = get_terms('paises');
		foreach($custom_terms as $custom_term) {
			wp_reset_query();
			$args = array('post_type' => 'sedes',
				'tax_query' => array(
					array(
						'taxonomy' => 'paises',
						'field' => 'slug',
						'terms' => $custom_term->slug,
						
					),
				),
			 );
		
			 $loop = new WP_Query($args);
			 if($loop->have_posts()) {
				 $sinespacio = str_replace('-', '_', $custom_term->slug);
				 $sinespacio = strtolower($sinespacio);
				  $sinespacio = removeAccents($sinespacio);
				 $agregado = $sinespacio;
				 $url_pag = get_bloginfo('stylesheet_directory');
								
				while($loop->have_posts()) : $loop->the_post();
				if (ICL_LANGUAGE_CODE == 'en'){
					$varmainoffice = "mainofficecolombia_en";
				}
				else{
					$varmainoffice = "oficinaprincipalcolombia";
				}
					$ciudadespacio = str_replace('-', '_', get_the_title());
					$pattern = '/[^a-zA-Z0-9]/';
					$ciudadespacio= preg_replace($pattern, '',$ciudadespacio);
					$ciudadespacio = strtolower($ciudadespacio);
					$ciudadespacio = $ciudadespacio.$agregado;
					$direccion= wpshed_get_custom_field( "wpshed_textfield_direccion");
					$telefono = wpshed_get_custom_field( "wpshed_textfield_telefono");
					$email = wpshed_get_custom_field( "wpshed_textfield_email");
					echo 'marker_'.$ciudadespacio.'.setMap(map);
						var boxText_'.$ciudadespacio.' = document.createElement("div");
						boxText_'.$ciudadespacio.'.style.cssText = "margin-top:-60px; margin-left:-90px; background: #4c4544; opacity:0.8; padding: 15px;";
						boxText_'.$ciudadespacio.'.id="info_mapa";
						boxText_'.$ciudadespacio.'.innerHTML = "<p>'.get_the_title().'</p><p>'.$direccion.'</p><p>'.$email.'</p><p><span>Tel:</span>'.$telefono.'</p>";
					var myOptions_'.$ciudadespacio.' = {
						 content: boxText_'.$ciudadespacio.'
						,disableAutoPan: false
						,maxWidth: 0
						,pixelOffset: new google.maps.Size(-140, 0)
						,zIndex: null
						,boxStyle: { 
							width: "280px"
						 }
						,closeBoxMargin: "-155px 4px 2px 2px"
						,closeBoxURL: "'.$url_pag.'/assets/map/closebutton.png"
						,infoBoxClearance: new google.maps.Size(1, 1)
						,isHidden: false
						,pane: "floatPane"
						,enableEventPropagation: false
					};
					
				
					var ib_'.$ciudadespacio.' = new InfoBox(myOptions_'.$ciudadespacio.');
														window.ib_'.$ciudadespacio.' = ib_'.$ciudadespacio.';
					google.maps.event.addListener(marker_'.$ciudadespacio.', "click", function(){
						
						 ib_'.$ciudadespacio.'.open(map, marker_'.$ciudadespacio.');
						});
						
						google.maps.event.addDomListener(window, "load", function ofpricipal(){
							ib_'.$varmainoffice.'.open(map, marker_'.$varmainoffice.');
						});
						';
				endwhile;
				
			 }
		}
?>
</script>