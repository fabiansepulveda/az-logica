<?php get_header(); ?>

<!-- inicio loop -->

<div id="fondo">


	<?php if ($cat==10) 
	{
		$noticia_dest = myprefix_get_option( 'select_noticia' );
		$query = new WP_Query( array (
		'p'  => $noticia_dest,
		'cat' => '10',
		'posts_per_page' => 1 )
		);

		if ( $query->have_posts() ) :
		while ( $query->have_posts() ) : $query->the_post();
		$url_thumb = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );?>
		<div class="image-news"><img id="imgimg" src="<?php echo $url_thumb; ?>" alt="Imagen Noticia Destacada" /></div>
		<div class="img_movil"><?php $image_auxiliar_news_1 = wp_get_attachment_image( get_post_meta( get_the_ID(), '_wpb_img_aux_id', 1 ), 'full', false, array( 'id' => 'imgimg_1' ) );
			echo $image_auxiliar_news_1; ?>
		</div>
		<div id="headerNoticias">
            <div class="container">
                <div class="row col-lg-offset-1">
                    <h1><?php the_title(); ?></h1>
                    <a href="<?php the_permalink() ?>"><?php echo __('Read More','azlogica');?></a>
                </div>
            </div>
		</div>
		<?php endwhile;
		wp_reset_postdata();
		else : ?>
		<p><?php _e( 'No tenemos Noticia Destacada.' ); ?></p>
		<?php endif; 
    } 
	elseif ($cat==11) {
		$blog_dest = myprefix_get_option( 'select_blog' );
		$query = new WP_Query( array (
		'p'  => $blog_dest,
		'cat' => '11',
		'posts_per_page' => 1 )
		);
		if ( $query->have_posts() ) :
		while ( $query->have_posts() ) : $query->the_post();
		$url_thumb = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );
		?>
		<div class="image-news"><img id="imgimg" src="<?php echo $url_thumb; ?>" /></div>
		<div class="img_movil"><?php $image_auxiliar_news_1 = wp_get_attachment_image( get_post_meta( get_the_ID(), '_wpb_img_aux_id', 1 ), 'full', false, array( 'id' => 'imgimg_1' ) );
			echo $image_auxiliar_news_1; ?>
		</div>
		<div id="headerNoticias">
            <div class="container">
                <div class="row col-lg-offset-1">
                    <h1><?php the_title(); ?></h1>
                    <a href="<?php the_permalink() ?>"><?php echo __('Read More','azlogica'); ?> </a>
                </div>
            </div>
		</div>
		<?php endwhile;
		wp_reset_postdata();
		else : ?>
		<p><?php _e( 'No tenemos Noticia Destacada.' ); ?></p>
		<?php endif; 
    } 
	else{
		global $post;
		$categories = get_the_category($post->ID);
		$cat_id=$categories[0]->term_id;
		query_posts( 'cat='.$cat_id.'&posts_per_page=1');	
		if ( have_posts() ) { while ( have_posts() ) { the_post();
		$url_thumb = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );
		 ?>
        
		<div class="image-news"><img id="imgimg" src="<?php echo $url_thumb; ?>" /></div>
		<div id="headerNoticias">
            <div class="container">
                <div class="row col-lg-offset-1">
                    <h1><?php the_title(); ?></h1>
                    <a href="<?php the_permalink() ?>"><?php echo __('Read More','azlogica');?></a>
                </div>
            </div>
		</div>
		<?php } // end while
		} // end if
    
    }?>
                
</div>

<!-- fin loop -->

<div id="paginaNoticias">
	<div id="content" class="container" >
		<div class="row breadcrumbs">
			<?php
				if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('<div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10">','</div>');
				}
			?>
		</div>
		<div class="row">
			<div class="col-xs-12">

				<!-- inicio loop -->
                
			<?php 

				global $post;
				$categories = get_the_category($post->ID);
				global $cat_id;
				$cat_id=$categories[0]->term_id;
				if ($cat==10){
					$destacado = myprefix_get_option( 'select_noticia' );
				} 
				elseif ($cat==11){
					$destacado = myprefix_get_option( 'select_blog' );
				}
				
				$args = array(
				'cat'=> $cat_id,
				'posts_per_page'    => 6,
				'post__not_in' => array($destacado)
				);
				query_posts( $args );
				if ( have_posts() ) { while ( have_posts() ) { the_post(); ?>
					<div class="col-sm-4 col-xs-12 noticia">
						<?php 
						$post_exclude[]=get_the_ID(); ?>
						<div class="image-news">
							<?php $image_auxiliar_news_1 = wp_get_attachment_image( get_post_meta( get_the_ID(), '_wpb_img_aux_id', 1 ), 'full' );
							echo $image_auxiliar_news_1; ?></div>
						<h3><?php the_title(); ?></h3>
						<p><?php echo substr(get_the_excerpt(), 0,160); ?></p>
						<a href="<?php the_permalink() ?>"><?php echo __('Read More','azlogica');?></a>
					</div>
				<?php } // end while
				
				} // end if
				
				?>
				<!-- fin loop -->
                
               

			</div>
			<div class="col-xs-12">
                <?php 
				array_push($post_exclude, $destacado);
				$features = $post_exclude;
				if($features){
					$postsNotIn = implode(",", $features); 
				} 
				$cat = get_category( get_query_var( 'cat' ) );
				$category = $cat->slug;
				echo do_shortcode('[ajax_load_more category="'.$category.'" orderby="date"  repeater="repeater2" exclude="'.$postsNotIn.'" posts_per_page="3" scroll="false" pause="true" transition="fade" button_label="Cargar más" ]');
				?>  
			</div>
		</div>
	</div>
</div>

<?php get_footer(); ?>