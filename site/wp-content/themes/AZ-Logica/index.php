<?php get_header(); 

?>
<div id="contenido">

	<div id="soluciones">
		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Sidebar Nuestras Soluciones") ) : ?>  
        <?php endif; ?>
        <div class="clear"></div>
		<div class="container">
			<div class="col-xs-12" role="tablist" id="myTab">
				<?php
					//list terms in a given taxonomy (useful as a widget for twentyten)
					$taxonomy = 'solucion_category';
					
					$tax_terms = get_terms($taxonomy,array(
						'hide_empty' => false,
					));
					echo '<ul>';
						$new_count=0;
						foreach ($tax_terms as $tax_term) {
						echo '<li>' . '<a data-toggle="tab" class="reload" role="tab" id="botonsol'.$new_count  .'" href="#sector'. $new_count  .'" title="' . $tax_term->name  . '" ' . '>' . $tax_term->name.'</a></li>';
						$new_count++;
						}
					echo '</ul>';
					?>
			</div><!--Category Soluciones-->
			<div class="clear"></div>
			<div class="tab-content">
				<?php
					$post_type = 'soluciones';
					$tax = 'solucion_category';
					$tax_terms = get_terms($tax);
					$count=0;
					if ($tax_terms) {
					  foreach ($tax_terms  as $tax_term) {
					    $args=array(
							'post_type' => $post_type,
							"$tax" => $tax_term->slug,
							'post_status' => 'publish',
							'posts_per_page' => -1,
							'caller_get_posts'=> 1

					    );
					    $my_query = null;
					    $my_query = new WP_Query($args);
					    if( $my_query->have_posts() ) {
					      echo '<div class="tab-pane fade in" id="sector'.$count.'">';
					      echo '<div id="slider_soluciones'.$count .'">';
					      while ($my_query->have_posts()) : $my_query->the_post(); 
					      	$image = wp_get_attachment_image( get_post_meta( get_the_ID(), '_wpb_icon_id', 1 ), 'full', false, array( 'class' => 'svg_featured' ) );
					        $eventname=$post->post_name; 
					        ?>
					        <div class="col-xs-2 solucion" >
								<a href="<?php the_permalink() ?>" id="<?php echo $eventname;  ?>" onclick="_gaq.push(['_trackEvent','Teaser','Click', '<?php echo $eventname; ?>-home']);">
									<span class="bola">
										<?php echo $image; ?>
									</span>
									<?php the_title( '<h2>', '</h2>' ); ?>
								</a>
							</div>
					        <?php
					      endwhile;
					      echo '</div></div>';
					    }
					    $count++;
					    wp_reset_query();
					  }
					}
				?>
			</div><!--End Tab-->
		</div><!-- End Container-->
	</div><!-- End Soluciones-->
	<div class="clear"></div>
	<div id="plataforma">
		<?php $query = new WP_Query( array ( 
									'orderby' => 'date', 
									'order' => 'ASC', 
									'cat' => '4' ));

		if ( $query->have_posts() ) : 
			echo '<h1 class="title_cat">' . get_cat_name(4) . '</h1>';
			echo '' . category_description(4) . '';
			echo '<div class="row" id="rowPlataforma">';
			while ( $query->have_posts() ) : $query->the_post(); ?>
				<div class="col-md-3 col-xs-6 plataforma">
					<?php $image_auxiliar = wp_get_attachment_image( get_post_meta( get_the_ID(), '_wpb_img_aux_id', 1 ), 'full' );
						echo $image_auxiliar;
					 the_title( '<h2>', '</h2>' );
					echo '<a href="' . get_permalink() . '">'; ?>
						
							<?php the_title( '<h2>', '</h2>' ); ?>
						
					</a>
				</div>
			<?php endwhile; 
			wp_reset_postdata();
		else : ?>
			<p><?php _e( 'No related posts.','azlogica'); ?></p>
		<?php endif; ?>
		</div><!-- End rowPlataforma -->
	</div><!-- End Plataforma-->
	<div class="clear"></div>
	
	<div id="clientes">
		<?php $query = new WP_Query( array ( 
                                    'orderby' => 'date', 
                                    'order' => 'DESC',
'posts_per_page' => -1, 
                                    'cat' => '9' )
                                );
        if ( $query->have_posts() ) :?>
        <h1 class="title_cat"><?php echo get_cat_name(9);?></h1>
        <div class="slider_clientes container">
            
            	
                <?php while ( $query->have_posts() ) : $query->the_post();?>
                <?php $link = get_post_meta($post->ID, '_wpb_link-client', true);?>
                <?php if ($link) {?>
                <div class="producto">
                <?php the_post_thumbnail('full'); ?>
                <a class="clientes-hover" target="_blank" href="<?php echo $link;?>"><span><?php _e('View more', 'azlogica');?></span></a>
                </div><!--endproducto-->
                <?php } else {?>
                <div class="producto">
                    <?php the_post_thumbnail('full'); ?>
                </div><!--endproducto-->
                <?php }?>
                <?php endwhile;
                wp_reset_postdata();?>
             
            
        </div>
        <?php else : ?>
        <p><?php _e('No related posts.','azlogica' ); ?></p>
        <?php endif; ?>


	</div><!-- End Clientes-->
	<div class="clear"></div>

	<div id="noticias">    
        <h1><?php _e('Latest','azlogica'); ?> <span> <?php _e('news','azlogica'); ?> </span></h1>
		<div class="container">
			<?php $noticia_dest = myprefix_get_option( 'select_noticia' );
			$query = new WP_Query( array (
								'p'  => $noticia_dest,
								'cat' => '10',
								'posts_per_page' => 1 )
							);
			if ( $query->have_posts() ) :
				while ( $query->have_posts() ) : $query->the_post();
					echo '<div class="col-xs-4 noticia">';
						echo '<div class="image-news col-xs-6 col-sm-12"><a href="' . get_permalink() . '">';
							$image_auxiliar_news = wp_get_attachment_image( get_post_meta( get_the_ID(), '_wpb_img_aux_id', 1 ), 'full' );
						echo $image_auxiliar_news;
						echo '</a></div>';
						echo '<div class="col-xs-6 col-sm-12">';
							echo '<a href="' . get_permalink() . '">';
								the_title( '<h3>', '</h3>' );
							echo '<p>' . substr(get_the_excerpt(), 0,165) . '</p>';?>
							</a>
							<a class="more" href="<?php echo get_permalink();?>"><?php _e('View more','azlogica');?></a>
						</div>
					</div>
				<?php endwhile;
					wp_reset_postdata();
					else : ?>
					<p><?php _e('No featured news.', 'azlogica' ); ?></p>
			<?php endif; ?>

			<?php $query = new WP_Query( array ( 
								'orderby' => 'date', 
								'order' => 'DSC', 
								'cat' => '10',
								'posts_per_page' => 2,
								'post__not_in' => array($noticia_dest) )
							);
			if ( $query->have_posts() ) :
				while ( $query->have_posts() ) : $query->the_post();
					echo '<div class="col-xs-4 noticia">';
						echo '<div class="image-news col-xs-6 col-sm-12"><a href="' . get_permalink() . '">';
							$image_auxiliar_news_1 = wp_get_attachment_image( get_post_meta( get_the_ID(), '_wpb_img_aux_id', 1 ), 'full' );
						echo $image_auxiliar_news_1;
						echo '</a></div>';
						echo '<div class="col-xs-6 col-sm-12">';
						echo '<a href="' . get_permalink() . '">';
							the_title( '<h3>', '</h3>' );
						echo '<p>' . substr(get_the_excerpt(), 0,165) . '</p>';?>
						</a>
							<a class="more" href="<?php echo get_permalink();?>"><?php _e('View more','azlogica');?></a>
						</div>
					</div>
				<?php endwhile;
					wp_reset_postdata();
					else : ?>
					<p><?php _e('No related news','azlogica'); ?></p>
			<?php endif; ?>
		</div>
	</div><!--End Noticias-->
	<div class="clear"></div>


	<div id="contactanos">
		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Sidebar Contactenos") ) : ?>  
        <?php endif; ?>
	</div>
	<div id="map-container" >
		<div id="map">
		</div>
		<div id="contact_form">
			<?php 
			if (ICL_LANGUAGE_CODE == 'en') {?>
				<?php echo do_shortcode( '[contact-form-7 id="623" title="Formulario de Contacto_en"  html_class="use-floating-validation-tip"]' ) ?>
			<?php } else {?>
				<?php echo do_shortcode( '[contact-form-7 id="4" title="Formulario de Contacto"  html_class="use-floating-validation-tip"]' ) ?>
			<?php }
			?>
		</div>
		<div id="sedes" >
			<div id="titulo_mapa"><p><?php _e('Our','azlogica'); ?> <span> <?php _e('Locations','azlogica'); ?> </span></p>
			</div>
			<?php
			$args = array(
				'orderby' => 'date', 
				'order' => 'ASC',
    		);
			$custom_terms = get_terms('paises', $args);
			echo '<ul id="list_sedes">';
			foreach($custom_terms as $custom_term) {
				wp_reset_query();
				$args = array(
					'orderby' => 'date', 
					'order' => 'DESC',
					'post_type' => 'sedes',
					'tax_query' => array(
						array(
							'orderby' => 'date', 
							'order' => 'DESC',
							'taxonomy' => 'paises',
							'field' => 'slug',
							'terms' => $custom_term->slug,
						),
					),
				);
				
				$loop = new WP_Query($args);	 
			 	if($loop->have_posts()) {
					$sinespacio = str_replace('-', '_', $custom_term->slug);
					$sinespacio = strtolower($sinespacio);
					$sinespacio =removeAccents ($sinespacio);
					echo '<li id="mapa_'.$sinespacio.'" class="ciudad">'.$custom_term->name.'</li>';
					/*
					echo '<div id="sedes_'.$sinespacio.'" class="sedes">';	
					
					echo '<ul>';				
					while($loop->have_posts()) : $loop->the_post();
						$ciudadespacio = str_replace(' ', '_', get_the_title());
						$ciudadespacio = strtolower($ciudadespacio);
						echo '<li id="sede_'.$ciudadespacio.'">'.get_the_title().'</li>';
					endwhile;
					echo '<ul>';
					
					//Fin Sedes
					echo '</div>';*/
					//Fin de la ciudad		
				}
					
			}
			echo "</ul>";
			?>
			<div id="representantes"><p><?php _e('Agents and dealers in','azlogica'); ?> <span> <?php _e('Latin America','azlogica'); ?> </span></p>
			</div>
		</div>
	</div>


</div>

<?php get_footer(); ?>