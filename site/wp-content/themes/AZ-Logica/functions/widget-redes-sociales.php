<?php
/*********************************************************/

/*Widget redes sociales*/

/*********************************************************/
class Redes_sociales extends WP_Widget {
	
	public function __construct( ){
		parent::__construct(
			'Redes_sociales',
			'Redes Sociales',
			array('description' => __('Redes Sociales', 'adaptive-framework'))
			);
	}
	public function form($instance){
		$defaults = array(
			'title' =>__('Titulo', 'adaptive-framework'),
			'ad_link' => 'https://www.dynamikcollective.com',
			'ad_img' => 'facebook.png'
		);
		
		$instance = wp_parse_args((array) $instance, $defaults);?>       
		<!--Imagen-->
        <p>
            <label for="<?php echo $this->get_field_id('ad_img')?>"><?php _e('Imagen', 'adaptive-framework'); ?></label>
            <input type="text" id="<?php echo $this->get_field_id('ad_img')?>" name="<?php echo $this->get_field_name('ad_img')?>" class="widefat" value="<?php echo esc_attr($instance['ad_img']); ?>" />
        </p>
        <!--Titulo-->
        <p>
        	<label for="<?php echo $this->get_field_id('title')?>"><?php _e('Título', 'adaptive-framework'); ?></label>
            <input type="text" id="<?php echo $this->get_field_id('title')?>" name="<?php echo $this->get_field_name('title')?>" class="widefat" value="<?php echo esc_attr($instance['title']); ?>" />
        </p>     
        <!--Link-->
        <p>
        	<label for="<?php echo $this->get_field_id('ad_link')?>"><?php _e('Link', 'adaptive-framework'); ?></label>
            <input type="text" id="<?php echo $this->get_field_id('ad_link')?>" name="<?php echo $this->get_field_name('ad_link')?>" class="widefat" value="<?php echo esc_attr($instance['ad_link']); ?>" />
        </p>
<?php	}
	public function update ($new_instance, $old_instance){
		$instance = $old_instance;
		
		//Title		
		$instance['title'] = $new_instance['title'];
		
		//the Ad
		$instance['ad_link'] = $new_instance ['ad_link'];
		$instance['ad_img'] = $new_instance ['ad_img'];
		
		return $instance;
	}
	
	public function widget($args, $instance){
		extract($args);
		
		$title = apply_filters('widget-title', $instance['title']);
		
		$ad_link = $instance['ad_link'];
		$ad_img = $instance['ad_img'];?>   
        
        <li>
        	<a class="icon" href="<?php echo($ad_link); ?>" target="_blank" title="<?php echo $title; ?>"><img src="<?php bloginfo ('wpurl'); ?>/wp-content/uploads/<?php echo $ad_img; ?>" title="<?php echo $title; ?>" alt="<?php echo $title; ?>" /></a>
        </li>
		<?php		
	}
}
register_widget('Redes_sociales');
?>