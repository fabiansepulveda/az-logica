<?php
/*********************************************************/

/*Widget Nuestras Soluciones*/

/*********************************************************/
class contactenos extends WP_Widget {
	
	public function __construct( ){
		parent::__construct(
			'contactenos',
			'Contactenos Info',
			array('description' => __('Descripción Contactenos.', 'adaptive-framework'))
			);
	}
	public function form($instance){
		$defaults = array(
			'title' =>__('Titulo', 'adaptive-framework'),
			'ad_description' => __('Descripción Contactenos. ','adaptive-framework')
		);
		
		$instance = wp_parse_args((array) $instance, $defaults);?>       
        <!--Titulo-->
        <p>
        	<label for="<?php echo $this->get_field_id('title')?>"><?php _e('Título', 'adaptive-framework'); ?></label>
            <input type="text" id="<?php echo $this->get_field_id('title')?>" name="<?php echo $this->get_field_name('title')?>" class="widefat" value="<?php echo esc_attr($instance['title']); ?>" />
        </p>     
        <!--Description-->
        <p>
        	<label for="<?php echo $this->get_field_id('ad_description')?>"><?php _e('Descripción', 'adaptive-framework'); ?></label>
            <textarea id="<?php echo $this->get_field_id('ad_description')?>" name="<?php echo $this->get_field_name('ad_description')?>" class="widefat"><?php echo esc_textarea($instance['ad_description']); ?>
            </textarea>
        </p>
<?php	}
	public function update ($new_instance, $old_instance){
		$instance = $old_instance;
		
		//Title		
		$instance['title'] = $new_instance['title'];
		
		//the Ad
		$instance['ad_description'] = $new_instance ['ad_description'];
		
		return $instance;
	}
	
	public function widget($args, $instance){
		extract($args);
		
		$title = apply_filters('widget-title', $instance['title']);
		
		$ad_description = $instance['ad_description'];?>   
        
        <h1>
        	<?php echo $title; ?>
        </h1>
        <p>
        	<?php echo($ad_description); ?>
        </p>
		<?php		
	}
}
register_widget('contactenos');
?>