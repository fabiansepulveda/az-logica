<?php get_header(); ?>
<div id="contenido">
	<div id="soluciones">
		<?php if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar("Sidebar Nuestras Soluciones") ) : ?>  
	    <?php endif; ?>
	    <div class="clear"></div>
		<div class="container">
			<div class="col-xs-12" role="tablist" id="myTab">
				<?php
					//list terms in a given taxonomy (useful as a widget for twentyten)
					$taxonomy = 'solucion_category';
					
					$tax_terms = get_terms($taxonomy );
					echo '<ul>';
						$new_count=0;
						foreach ($tax_terms as $tax_term) {
						echo '<li>' . '<a data-toggle="tab" class="reload" role="tab" id="botonsol'.$new_count  .'" href="#sector'. $new_count  .'" title="' . $tax_term->name  . '" ' . '>' . $tax_term->name.'</a></li>';
						$new_count++;
						}
					echo '</ul>';
					?>
			</div><!--Category Soluciones-->
			<div class="clear"></div>
			<div class="tab-content">
				<?php
					$post_type = 'soluciones';
					$tax = 'solucion_category';
					$tax_terms = get_terms($tax);
					$count=0;
					if ($tax_terms) {
					  foreach ($tax_terms  as $tax_term) {
					    $args=array(
							'post_type' => $post_type,
							"$tax" => $tax_term->slug,
							'post_status' => 'publish',
							'posts_per_page' => -1,
							'caller_get_posts'=> 1

					    );
					    $my_query = null;
					    $my_query = new WP_Query($args);
					    if( $my_query->have_posts() ) {
					      echo '<div class="tab-pane fade in" id="sector'.$count.'" alt="'.$tax_term->name.'">';
					      echo '<div id="slider_soluciones'.$count .'">';
					      while ($my_query->have_posts()) : $my_query->the_post(); 
					      	$image = wp_get_attachment_image( get_post_meta( get_the_ID(), '_wpb_icon_id', 1 ), 'full', false, array( 'class' => 'svg_featured' ) );
					        $eventname=$post->post_name; 
					        ?>
					        <div class="col-xs-2 solucion">
								<a href="<?php the_permalink() ?>" id="<?php echo $eventname;  ?>" onclick="_gaq.push(['_trackEvent','Teaser','Click', '<?php echo $eventname; ?>-archive']);">
									<span class="bola">
										<? echo $image; ?>
									</span>
									<?php the_title( '<h2>', '</h2>' ); ?>
								</a>
							</div>
					        <?php
					      endwhile;
					      echo '</div></div>';
					    }
					    $count++;
					    wp_reset_query();
					  }
					}
				?>
			</div><!--End Tab-->
		</div><!-- End Container-->
	</div><!-- End Soluciones-->
	</div><!--End Content-->
<?php get_footer(); ?>