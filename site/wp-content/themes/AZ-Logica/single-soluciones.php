<?php get_header(); ?>

<div id="fondo">
		<?php $url_thumb = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) ); ?>
		<img id="imgimg" src="<?php echo $url_thumb; ?>" />
	<div class="container tituloInterna">
		<div class="row col-lg-offset-1">
			<h1>
				<?php echo get_the_title( $post_id ); ?>
			</h1>
		</div>
	</div>
</div>

<div id="paginaInternaSoluciones">

	<div class="container">

			<div class="row breadcrumbs">
				<?php
					if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb('<div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10">','</div>');
					}
				?>
			</div>

			<div class="col-sm-10 col-xs-12 noPadding col-lg-offset-1">

				<div class="col-sm-9  col-xs-12  noPadding" id="contPost">

					<h2 class="subtitulo">
						<?php echo get_the_title( $post_id ); ?>
					</h2>
					<?php the_content(); ?>

				</div>

				<div class="col-sm-3 col-xs-12" id="sidebar">
					
					
					<?php  
					 $terms = get_the_terms( $post->ID , 'solucion_category' );
					 if ( $terms != null ){
					 foreach( $terms as $term ) {
					 /*print $term->slug ;*/
					 global $currenttrm;
					 $currenttrm=$term->slug;
						 if ($term->slug=='activo')
						{
						echo '<h2 class="sidebar">
								<span>otros activos</span>
								a monitorear 
							  </h2>';
						 }
						 else if ($term->slug=='sector')
						{
						echo '<h2 class="sidebar">
								<span>otros</span> sectores
							  </h2>';
						 }
						 else if ($term->slug=='caso-de-uso')
						{
						echo '<h2 class="sidebar">
								<span>otros casos</span> 
								<br>de uso en línea
							  </h2>';
						 }
					 unset($term);
					} }
					
					?>
                    
                  
					 <ul class="casoenlinea">
						<?php $taxonomy = 'solucion_category';
                        $query = NULL;
                        $terms = wp_get_post_terms($post->ID, $taxonomy);
                        if ($terms) {  
							$term_ids = array();  
							foreach($terms as $individual_term) $term_ids[] = $individual_term->term_id;  
							$args=array(  
										'post_type' => get_post_type(),
										'tax_query' => array(
														array(
															'taxonomy'  => $taxonomy,
															'field'     => 'id',
															'terms'     => $term_ids,
															'operator'  => 'IN'
															) 
															),
										'post__not_in' => array($post->ID),  
										'showposts' => $count, 
										'caller_get_posts' => 1  
										);  
							$the_query = new WP_Query( $args );
                        }
                        if ( $the_query->have_posts() ) {
							while ( $the_query->have_posts() ) {
							$the_query->the_post();
							?>
							<li>
							<div class="col-xs-12 solucion">
							<a href="<?php the_permalink() ?>">
							<div class="bola">
							<?php $image = wp_get_attachment_image( get_post_meta( get_the_ID(), '_wpb_icon_id', 1 ), 'full' ); echo $image;?> 
							</div>
							<h2><?php echo get_the_title( $post_id ); ?></h2>
							</a>
							</div>	
							</li>  
							<?php
                        }
                        
                        } else {
                        // no posts found
                        }
                        wp_reset_postdata();?>
                        <?php wp_reset_query(); ?>
					</ul>
	            </div>
	        </div>
	    </div>
	</div>
	<div id="masSoluciones">
			<div class="container">
				<div class="col-sm-10 col-xs-12 col-lg-push-1">
					<div class="col-sm-6 col-xs-10">
						<?php  
                         $terms = get_the_terms( $post->ID , 'solucion_category' );
                         if ( $terms != null ){
                         foreach( $terms as $term ) {
							  if ($currenttrm=='sector')
								{
							 		echo '<h2>
									<span> activo</span> a monitorear 
									</h2>';
								}
								else if ($currenttrm=='activo')
								{
							 		echo '<h2>
									<span> sector</span>
									</h2>';
								}
								else if ($currenttrm=='caso-de-uso')
								{
							 		echo '<h2>
									<span>sector</span>
									</h2>';
								}
                         
                         unset($term);
                        } }
                        ?>
						
						<ul class="sector">
							<?php
									 
							 $terms = get_the_terms( $post->ID , 'solucion_category' );
							 if ( $terms != null ){
							 foreach( $terms as $term ) {
							 /*print $term->slug ;*/
								 if ($currenttrm=='sector')
								{
									$args=array(
					           'post_type' => 'soluciones',
						       'taxonomy'=>'solucion_category',
					           'term' => 'activo',
						       'post__not_in' => $excludes,
					           'post_status' => 'publish'
					                    );
								
								 }
								 else  if ($currenttrm=='activo')
								{
									$args=array(
					           'post_type' => 'soluciones',
						       'taxonomy'=>'solucion_category',
					           'term' => 'sector',
						       'post__not_in' => $excludes,
					           'post_status' => 'publish'
					                    );
								
								 }
								 else  if ($currenttrm=='caso-de-uso')
								{
									$args=array(
					           'post_type' => 'soluciones',
						       'taxonomy'=>'solucion_category',
					           'term' => 'sector',
						       'post__not_in' => $excludes,
					           'post_status' => 'publish'
					                    );
								
								 }
								 
							 unset($term);
							} }
					          query_posts( $args ); ?>	
					          <?php while (have_posts()) : the_post(); ?>
					         
					          	<li class="col-xs-4 solucion">
					          		
										<a href="<?php the_permalink() ?>">
											<div class="bola">
												<?php $image = wp_get_attachment_image( get_post_meta( get_the_ID(), '_wpb_icon_id', 1 ), 'full' ); echo $image;?> 
											</div>
											<h3><?php echo get_the_title( $post_id ); ?></h3>
										</a>
					
						         
						        </li>  
							
							<?php endwhile; 
                            wp_reset_postdata();?>
                            <?php wp_reset_query(); ?>
						</ul>
					</div>
					<div class="col-sm-6 col-xs-10">
						<?php  
                         $terms = get_the_terms( $post->ID , 'solucion_category' );
                         if ( $terms != null ){
                         foreach( $terms as $term ) {
							  if ($currenttrm=='sector')
								{
							 		echo '<h2>
									<span> Casos </span>de uso en línea
									</h2>';
								}
								else if ($currenttrm=='activo')
								{
							 		echo '<h2>
									<span>Casos de </span>uso en línea
									</h2>';
								}
								else if ($currenttrm=='caso-de-uso')
								{
							 		echo '<h2>
									<span> Activo </span>a monitorear
									</h2>';
								}
                         
                         unset($term);
                        } }
                        ?>
						
						<ul class="sector">
							<?php
									 
							 $terms = get_the_terms( $post->ID , 'solucion_category' );
							 if ( $terms != null ){
							 foreach( $terms as $term ) {
							 /*print $term->slug ;*/
								 if ($currenttrm=='sector')
								{
									$args=array(
					           'post_type' => 'soluciones',
						       'taxonomy'=>'solucion_category',
					           'term' => 'caso-de-uso',
						       'post__not_in' => $excludes,
					           'post_status' => 'publish'
					                    );
								
								 }
								 else  if ($currenttrm=='activo')
								{
									$args=array(
					           'post_type' => 'soluciones',
						       'taxonomy'=>'solucion_category',
					           'term' => 'caso-de-uso',
						       'post__not_in' => $excludes,
					           'post_status' => 'publish'
					                    );
								
								 }
								 else  if ($currenttrm=='caso-de-uso')
								{
									$args=array(
					           'post_type' => 'soluciones',
						       'taxonomy'=>'solucion_category',
					           'term' => 'activo',
						       'post__not_in' => $excludes,
					           'post_status' => 'publish'
					                    );
								
								 }
								 
							 unset($term);
							} }
					          query_posts( $args ); ?>	
					          <?php while (have_posts()) : the_post(); ?>
					         
					          	<li class="col-xs-4 solucion">
					          		
										<a href="<?php the_permalink() ?>">
											<div class="bola">
												<?php $image = wp_get_attachment_image( get_post_meta( get_the_ID(), '_wpb_icon_id', 1 ), 'full' ); echo $image;?> 
											</div>
											<h3><?php echo get_the_title( $post_id ); ?></h3>
										</a>
					
						         
						        </li>  
							
							<?php endwhile; 
                            wp_reset_postdata();?>
                            <?php wp_reset_query(); ?>
						</ul>
					</div>
				</div>
			</div>
	</div>
</div>
<?php get_footer(); ?>