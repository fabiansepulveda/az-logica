<?php get_header(); ?>

<div id="paginaNoticiasInterna">
	<!--newbanner-->
	<div id="fondo">
		
		<?php if ( have_posts() ) while ( have_posts() ) : the_post();
		$url_thumb = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) ); ?>
		<div class="image-news"  ><img id="imgimg" src="<?php echo $url_thumb; ?>" /></div>
		<?php endwhile; // end of the loop. ?>
		
				<div id="headerNoticias">
		            <div class="container">
		                <div class="row col-lg-offset-1">
		                    <h1><span><?php the_title(); ?></span></h1>
		                    <!--<p><?php echo substr(get_the_excerpt(), 0,160); ?></p>-->
		                  
		                </div>
		            </div>
				</div>
	</div>
	<!--endnewbanner-->
	<div class="container">
			<div class="row col-lg-offset-1">

				<!--<div class="col-xs-10 internaNoticias">
					<?php the_post_thumbnail(); ?> 
					<div class="infoNoticia">
						<h1>
							<?php echo the_title(); ?>
						</h1>
						<p><?php echo substr(get_the_excerpt(), 0,160); ?></p>
					</div>
					</div>-->
			</div>
			<div class="row breadcrumbs">
				<?php
					if ( function_exists('yoast_breadcrumb') ) {
						yoast_breadcrumb('<div class="col-xs-12 col-sm-12 col-md-offset-1 col-md-10">','</div>');
					}
				?>
			</div>
			<div class="row col-lg-offset-1">
            
				<div class="col-sm-7 col-xs-10" id="contPost">
                	<div class="sharet top">
                        <span class='st_sharethis_large' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>'></span>
                        <span class='st_facebook_large' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>'></span>
                        <span st_via='@AZLOGICA' st_username='daniela.castro@azlogica.com' class='st_twitter_large' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>'></span>
                        <span class='st_googleplus_large' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>'></span>
                    </div>
					<?php the_content();?>
                    <div class="sharet">
                        <span class='st_sharethis_large' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>'></span>
                        <span class='st_facebook_large' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>'></span>
                        <span st_via='@AZLOGICA' st_username='daniela.castro@azlogica.com' class='st_twitter_large' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>'></span>
                        <span class='st_googleplus_large' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>'></span>
                    </div>
                    <div class="clear"></div>
                    <?php if (in_category('blog')){ ?>
                        <div class="comments">
                        <?php // If comments are open or we have at least one comment, load up the comment template.
                        if ( comments_open() || get_comments_number() ) {
                            comments_template();
                        } ?>
                    </div>
                    <?php } ?>                    
				</div>
				<div class="col-sm-3 col-xs-10" id="sidebar">
					<h2 class="sidebar">
						<?php _e( '<span>Related</span> posts','azlogica'); ?>
					</h2>
								<?php 
								global $post;
								$currentpostid = $post->ID; 
                                $categories = get_the_category($post->ID);
                                $cat_id=$categories[0]->term_id;
                                if ($cat_id==10 || $cat_id==38){
                                $destacado = myprefix_get_option( 'select_noticia' );
                                } 
                                elseif ($cat_id==11){
                                $destacado = myprefix_get_option( 'select_blog' );
                                }
                                
                                if($currentpostid!=$destacado){



	                                $query = new WP_Query( array (
	                                'p'  => $destacado,
	                                'posts_per_page' => 1 )
	                                );
	                                if ( $query->have_posts() ) :
	                                while ( $query->have_posts() ) : $query->the_post();?>
	                                <div class="masNoticias">
	                                    <a href="<?php the_permalink() ?>">
	                                    <?php $image_auxiliar_news_1 = wp_get_attachment_image( get_post_meta( get_the_ID(), '_wpb_img_aux_id', 1 ), 'full' );
											echo $image_auxiliar_news_1; ?>
	                                    <h4><?php the_title(); ?></h4>
	                                    </a>
	                                </div>
	                                <?php endwhile;
	                                wp_reset_postdata();
	                                else : ?>
	                                <p><?php _e( 'No tenemos Noticia Destacada.' ); ?></p>
	                                <?php endif; ?>
	                                 
				
									
	                                
									<?php 
									global $post;
									$categories = get_the_category($post->ID);
									$cat_id=$categories[0]->term_id;
									$postID = get_the_ID();
									$args2 = array(
									'cat'=> $cat_id,
									'posts_per_page'    => -1,
									'orderby'    => rand,
									'post__not_in' => array($destacado)
									);
									query_posts( $args2 );
									$contador=0;

									if ( have_posts() ) 
										{
										 while ( have_posts() ) 
										 	{ 
										 		
										 		the_post(); 
										 		if($postID!=get_the_ID() && $contador<2)
										 		{
										 			$contador+=1;
										 		?>
									<div class="masNoticias">
										<a href="<?php the_permalink() ?>">
											<?php $image_auxiliar_news_1 = wp_get_attachment_image( get_post_meta( get_the_ID(), '_wpb_img_aux_id', 1 ), 'full' );
											echo $image_auxiliar_news_1; ?>
											<h4><?php the_title(); ?></h4>
										</a>
									</div>
									<?php 
									}//end if post
									} // end while
									} // end if have

								}else{
									$categories = get_the_category($post->ID);
									$cat_id=$categories[0]->term_id;
									$postID = get_the_ID();
									$args2 = array(
									'cat'=> $cat_id,
									'posts_per_page'    => -1,
									'orderby'    => rand,
									'post__not_in' => array($destacado)
									);
									query_posts( $args2 );
									$contador=0;

									if ( have_posts() ) 
										{
										 while ( have_posts() ) 
										 	{ 
										 		
										 		the_post(); 
										 		if($postID!=get_the_ID() && $contador<3)
										 		{
										 			$contador+=1;
										 		?>
									<div class="masNoticias">
										<a href="<?php the_permalink() ?>">
											<?php $image_auxiliar_news_1 = wp_get_attachment_image( get_post_meta( get_the_ID(), '_wpb_img_aux_id', 1 ), 'full' );
												echo $image_auxiliar_news_1; ?>
											<h4><?php the_title(); ?></h4>
										</a>
									</div>
									<?php 
									}//end if post
									} // end while

								}
							}
								?>
							</h4>
						</a>
					</div>
				</div>
			</div>
	</div>
</div>

<?php get_footer(); ?>