<?php
/********** OPCIONES DEL TEMA ********/
function wpb_initialize_cmb_meta_boxes() {
  if ( ! class_exists( 'cmb_Meta_Box' ) )
    require_once(get_template_directory() . '/lib/CMB/init.php');
}
require_once ( get_template_directory() . '/admin/theme-options.php' );
/* Logo personalizado en el admin */
function my_login_logo() { ?>
    <style type="text/css">
        body.login div#login h1 a {
          background-image: url(<?php echo myprefix_get_option( 'logo' ); ?>);
          padding-bottom: 30px;
          width: 100%;
          height: 30px;
          background-size: initial;
        }
		    body.login div#login form#loginform p.submit input#wp-submit {
    		  background-color: #FFD400;
    		  border: none;
    		  color: #033C1C;
    		}
    </style>
	<?php
}
add_action( 'login_enqueue_scripts', 'my_login_logo' );

/*function my_login_logo_url() {
    return 'http://www.google.com.co';
}
add_filter( 'login_headerurl', 'my_login_logo_url' );*/

function my_login_logo_url_title() {
    return 'AZ Lógica';
}
add_filter( 'login_headertitle', 'my_login_logo_url_title' );

/*****Registro Menus*******/
function register_my_menus() {
  register_nav_menus(
    array(
      'main-menu' => __( 'Main Menu' )
    )
  );
}
add_action( 'init', 'register_my_menus' );

/******** SVG *******/
function cc_mime_types( $mimes ){
  $mimes['svg'] = 'image/svg+xml';
  return $mimes;
}
add_filter( 'upload_mimes', 'cc_mime_types' );

/***********SOPORTE PARA IMAGENES EN EL POST Y REMOVER LOS ATRIBUTOS WIDHT & HEIGHT*************************/
if (function_exists('add_theme_support')) { add_theme_support('post-thumbnails'); }
add_filter( 'post_thumbnail_html', 'remove_width_attribute', 10 );
add_filter( 'image_send_to_editor', 'remove_width_attribute', 10 );

function remove_width_attribute( $html ) {
   $html = preg_replace( '/(width|height)="\d*"\s/', "", $html );
   return $html;
}

/******** SOLUCIONES **********/
add_action( 'init', 'create_brand' );
function create_brand() {
    register_post_type( 'soluciones',
        array(
            'labels' => array(
                'name' => 'Soluciones',
                'singular_name' => 'Soluciones',
                'add_new' => 'Añadir nueva Solución',
                'add_new_item' => 'Añadir nueva Solución',
                'edit' => 'Editar',
                'edit_item' => 'Editar Solución',
                'new_item' => 'Nueva Solución',
                'view' => 'Ver',
                'view_item' => 'Ver Solución',
                'search_items' => 'Buscar Solución',
                'not_found' => 'Solución no encontrada',
                'not_found_in_trash' => 'Solución no encontrada en papelera',
                'parent' => 'Parent Solución Review'
            ),
            'public' => true,
            'menu_position' => 2,
            'supports' => array( 'title', 'editor', 'custom-fields', 'page-attributes', 'thumbnail' ),
            'taxonomies' => array( 'solucion_category' ),
            'hierarchical' => true,
            'menu_icon' => get_bloginfo('stylesheet_directory').'/assets/soluciones.png',
            'has_archive' => true
        )
    );
}
///******************///
function my_taxonomies_product() {
  $labels = array(
    'name'              => _x( 'Categoria', 'taxonomy general name' ),
    'singular_name'     => _x( 'Categoria', 'taxonomy singular name' ),
    'search_items'      => __( 'Buscar en Categorias' ),
    'all_items'         => __( 'Todas las Categorias' ),
    'parent_item'       => __( 'Categoria Soluciones' ),
    'parent_item_colon' => __( 'Parent Product Category:' ),
    'edit_item'         => __( 'Editar Categoria' ), 
    'update_item'       => __( 'Actualizar Categoria' ),
    'add_new_item'      => __( 'Añadir Categoria' ),
    'new_item_name'     => __( 'Nueva Categoria' ),
    'menu_name'         => __( 'Categorias' ),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => true,
  );
  register_taxonomy( 'solucion_category', 'soluciones', $args );
}
add_action( 'init', 'my_taxonomies_product', 0 );


/********* Excerpt Page **************/
add_action( 'init', 'my_add_excerpts_to_pages' );
function my_add_excerpts_to_pages() {
     add_post_type_support( 'soluciones', 'excerpt' );
     add_post_type_support( 'page', 'excerpt' );
}
/********SIDEBAR REGISTER******************/
if ( function_exists('register_sidebar') )  

  register_sidebar(
    array(  
      'name' => __('Sidebar redes sociales', 'adaptive-framework'),
      'id' => 'sidebar-redes-sociales',  
    )
  );
  register_sidebar(
    array(  
      'name' => __('Sidebar Nuestras Soluciones', 'adaptive-framework'),
      'id' => 'sidebar-nuestras-soluciones',  
    )
  );
  register_sidebar(
    array(  
      'name' => __('Sidebar Contactenos', 'adaptive-framework'),
      'id' => 'sidebar-contactenos',  
    )
  );
  register_sidebar(
    array(  
      'name' => __('Sidebar Twitter', 'adaptive-framework'),
      'id' => 'sidebar-twitter',  
    )
  );
  
  

/*********Widgets Registers**************/
require_once('functions/widget-redes-sociales.php');
require_once('functions/widget-soluciones-logisticas.php');
require_once('functions/widget-contactenos.php');

/**********Paises Sedes******************/

add_action( 'init', 'create_sede' );
function create_sede() {
    register_post_type( 'sedes',
        array(
            'labels' => array(
                'name' => 'Sedes',
                'singular_name' => 'Sedes',
                'add_new' => 'Añadir nueva Sede',
                'add_new_item' => 'Añadir nueva Sede',
                'edit' => 'Editar',
                'edit_item' => 'Editar sede',
                'new_item' => 'Nueva sede',
                'view' => 'Ver',
                'view_item' => 'Ver sede',
                'search_items' => 'Buscar sede',
                'not_found' => 'sede no encontrada',
                'not_found_in_trash' => 'Sede no encontrada en papelera',
                'parent' => 'Parent Movie Review'
            ),
            'public' => true,
            'menu_position' => 2,
            'supports' => array( 'title', 'editor', 'custom-fields', 'page-attributes' ),
			'taxonomies' => array( 'paises' ),
			'hierarchical' => true,
            'menu_icon' => get_bloginfo('stylesheet_directory').'/assets/map/map.png',
            'has_archive' => true
        )
    );
}
/*********Paises Taxonomia***************///
function my_countries() {
  $labels = array(
    'name'              => _x( 'Paises', 'taxonomy general name' ),
    'singular_name'     => _x( 'Paises', 'taxonomy singular name' ),
    'search_items'      => __( 'Buscar en Paises' ),
    'all_items'         => __( 'Todas los Paises' ),
    'parent_item'       => __( 'Paises' ),
    'parent_item_colon' => __( 'Parent Product Category:' ),
    'edit_item'         => __( 'Editar Pais' ), 
    'update_item'       => __( 'Actualizar Pais' ),
    'add_new_item'      => __( 'Añadir Pais' ),
    'new_item_name'     => __( 'Nuevo Pais' ),
    'menu_name'         => __( 'Paises' ),
  );
  $args = array(
    'labels' => $labels,
    'hierarchical' => true,
  );
  register_taxonomy( 'paises', 'sedes', $args );
}
add_action( 'init', 'my_countries', 0 );

//AQUI EMPIEZA EL PHP IMAGE TAXONOMY

add_action ( 'paises_edit_form_fields', 'campos_extras');
function campos_extras( $tag ) {  
$t_id = $tag->term_id;
$cat_meta = get_option( "category_$t_id");
?>
<tr class="form-field">
<th scope="row" valign="top"><label for="cat_Image_url"><?php _e('Url de la Imagen'); ?></label></th>
<td>
<input type="text" name="Cat_meta[img]" id="Cat_meta[img]" size="3" style="width:60%;" value="<?php echo $cat_meta['img'] ? $cat_meta['img'] : ''; ?>"><br />
<span class="description"><?php _e('Imagen para la categoría, usar http://'); ?></span>
</td>
</tr>
<?php
}
add_action ( 'edited_paises', 'guardar_campos_extras');

function guardar_campos_extras( $term_id ) {
    if ( isset( $_POST['Cat_meta'] ) ) {
    $t_id = $term_id;
    $cat_meta = get_option( "category_$t_id");
    $cat_keys = array_keys($_POST['Cat_meta']);
    foreach ($cat_keys as $key){
        if (isset($_POST['Cat_meta'][$key])){
         $cat_meta[$key] = $_POST['Cat_meta'][$key];
        }
    }
    update_option( "category_$t_id", $cat_meta );
    }
}


//AQUI TERMINA  EL PHP IMAGE TAXONOMY



//***************METABOX INFO SEDES***********//


// Little function to return a custom field value
function wpshed_get_custom_field( $value ) {
	global $post;

    $custom_field = get_post_meta( $post->ID, $value, true );
    if ( !empty( $custom_field ) )
	    return is_array( $custom_field ) ? stripslashes_deep( $custom_field ) : stripslashes( wp_kses_decode_entities( $custom_field ) );

    return false;
}

// Register the Metabox
function wpshed_add_custom_meta_box() {
	add_meta_box( 'wpshed-meta-box', __( 'Información de la Sede', 'azlogica' ), 'wpshed_meta_box_output', 'sedes', 'normal', 'high' );
}
add_action( 'add_meta_boxes', 'wpshed_add_custom_meta_box' );

// Output the Metabox
function wpshed_meta_box_output( $post ) {
	// create a nonce field
	wp_nonce_field( 'my_wpshed_meta_box_nonce', 'wpshed_meta_box_nonce' ); ?>
	<div style="display: inline-block; width:500px;">
	<p>
		<label for="wpshed_textfield_direccion"><?php _e( 'Dirección', 'azlogica' ); ?></label></br>
		<input type="text" name="wpshed_textfield_direccion" id="wpshed_textfield_direccion" value="<?php echo wpshed_get_custom_field( 'wpshed_textfield_direccion' ); ?>" size="50" />
    </p>
	<p>
		<label for="wpshed_textfield_telefono"><?php _e( 'Teléfono', 'azlogica' ); ?></label></br>
		<input type="text" name="wpshed_textfield_telefono" id="wpshed_textfield_telefono" value="<?php echo wpshed_get_custom_field( 'wpshed_textfield_telefono' ); ?>" size="50" />
    </p>
	<p>
		<label for="wpshed_textfield_email"><?php _e( 'Email', 'azlogica' ); ?></label></br>
		<input type="text" name="wpshed_textfield_email" id="wpshed_textfield_email" value="<?php echo wpshed_get_custom_field( 'wpshed_textfield_email' ); ?>" size="50" />
    </p>
	</div>
	<div style="display: inline-block; width:500px;">
	<h2>Coordenadas</h2>
	<p>
		<label for="wpshed_textfield_lat"><?php _e( 'Latitud', 'azlogica' ); ?></label></br>
		<input type="text" name="wpshed_textfield_lat" id="wpshed_textfield_lat" value="<?php echo wpshed_get_custom_field( 'wpshed_textfield_lat' ); ?>" size="50" />
    </p>
	<p>
		<label for="wpshed_textfield_long"><?php _e( 'Longitud', 'azlogica' ); ?></label></br>
		<input type="text" name="wpshed_textfield_long" id="wpshed_textfield_long" value="<?php echo wpshed_get_custom_field( 'wpshed_textfield_long' ); ?>" size="50" />
    </p>
	
	
	</div>
    
	<?php
}

// Save the Metabox values
function wpshed_meta_box_save( $post_id ) {
	// Stop the script when doing autosave
	if( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) return;

	// Verify the nonce. If insn't there, stop the script
	if( !isset( $_POST['wpshed_meta_box_nonce'] ) || !wp_verify_nonce( $_POST['wpshed_meta_box_nonce'], 'my_wpshed_meta_box_nonce' ) ) return;

	// Stop the script if the user does not have edit permissions
	if( !current_user_can( 'edit_post' ) ) return;

    // Save the textfield
	if( isset( $_POST['wpshed_textfield_direccion'] ) )
		update_post_meta( $post_id, 'wpshed_textfield_direccion', esc_attr( $_POST['wpshed_textfield_direccion'] ) );
		// Save the textfield
	if( isset( $_POST['wpshed_textfield_telefono'] ) )
		update_post_meta( $post_id, 'wpshed_textfield_telefono', esc_attr( $_POST['wpshed_textfield_telefono'] ) );
		// Save the textfield
	if( isset( $_POST['wpshed_textfield_email'] ) )
		update_post_meta( $post_id, 'wpshed_textfield_email', esc_attr( $_POST['wpshed_textfield_email'] ) );
	if( isset( $_POST['wpshed_textfield_lat'] ) )
		update_post_meta( $post_id, 'wpshed_textfield_lat', esc_attr( $_POST['wpshed_textfield_lat'] ) );
	if( isset( $_POST['wpshed_textfield_long'] ) )
		update_post_meta( $post_id, 'wpshed_textfield_long', esc_attr( $_POST['wpshed_textfield_long'] ) );


}
add_action( 'save_post', 'wpshed_meta_box_save' );



function mytheme_admin_load_scripts($hook) {
    if( $hook != 'post.php' && $hook != 'post-new.php') 
        return;
    wp_enqueue_script( 'custom-js', get_template_directory_uri()."/js/option-admin.js" );
}
add_action('admin_enqueue_scripts', 'mytheme_admin_load_scripts');

/*Move comments form*/
function wpb_move_comment_field_to_bottom( $fields ) {
    $comment_field = $fields['comment'];
    unset( $fields['comment'] );
    $fields['comment'] = $comment_field;
    return $fields;
}

add_filter( 'comment_form_fields', 'wpb_move_comment_field_to_bottom' );
?>