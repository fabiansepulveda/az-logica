<?php /*
Template Name: Tag Archive
*/ ?>
<?php get_header(); ?>
<div id="content" class="container tag">
	<div class="row">
		<div class="col-xs-12">
			<h2> Etiquetas Útiles </h2>
			<div class="navigation">
				<div class="alignleft"><?php next_posts_link( '« Entradas Antiguas' ); ?></div>
				<div class="alignright"><?php previous_posts_link( 'Entradas Recientes»' ); ?></div>
			</div>
			<?php if ( have_posts() ) : ?>
				<ul>
					<?php while ( have_posts() ) : the_post(); ?>
						<li><a href="<?php the_permalink(); ?>" rel="bookmark"><?php the_title(); ?></a></li>
					<?php endwhile; ?>
				</ul>
			<?php endif; ?>
		</div><!-- Col xs 12-->
	</div><!-- Row-->
</div><!-- Container-->
<?php get_footer(); ?>