<?php
/*
Plugin Name: Metabox Plugin
Description: Crea los metabox para el sitio Timon
Version: 0.1
Author: Dynamik Collective
Author URI: http://www.dynamikcollective.com/
*/

//Initialize the metabox class

add_action( 'init', 'wpb_initialize_cmb_meta_boxes', 9999 );

//Add Meta Boxes

function wpb_sample_metaboxes( $meta_boxes ) {
	$prefix = '_wpb_'; // Prefix for all fields

	$meta_boxes['test_metabox'] = array(
		'id' => 'test_metabox',
		'title' => 'Icono',
		'pages' => array('soluciones'), // post type
		'context' => 'side',
		'priority' => 'core',
		'show_names' => true, // Show field names on the left
		'fields' => array(
			array(
		    'name' => 'Icono Solución',
		    'desc' => 'Cargar un icono en formato svg',
		    'default' => '',
		    'id' => $prefix . 'icon',
		    'type' => 'file'
		),
		),
	);
    $meta_boxes['metabox_post'] = array(
        'id' => 'metabox_post',
        'title' => 'Imagen Auxiliar',
        'pages' => array('post'), // post type
        'context' => 'side',
        'priority' => 'core',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            array(
            'name' => 'Imágen auxiliar del post',
            'desc' => 'jpg,png',
            'default' => '',
            'id' => $prefix . 'img_aux',
            'type' => 'file'
        ),
        ),
    );
    $meta_boxes['metabox_link'] = array(
        'id' => 'metabox_link',
        'title' => 'link del sitio del cliente',
        'pages' => array('post'), // post type
        'context' => 'side',
        'priority' => 'core',
        'show_names' => true, // Show field names on the left
        'fields' => array(
            array(
            'name' => 'Link',
            'desc' => 'Ingrese la url del cliente',
            'default' => '',
            'id' => $prefix . 'link-client',
            'type' => 'text_url'
        ),
        ),
    );

	return $meta_boxes;
}
add_filter( 'cmb_meta_boxes', 'wpb_sample_metaboxes' );

function cmb_taxonomy_meta_initiate() {

	require_once( 'Taxonomy_MetaData/Taxonomy_MetaData_CMB.php' );

	/**
	 * Semi-standard CMB metabox/fields array
	 */
	$meta_box = array(
		'id'         => 'cat_options',
		'show_on'    => array( 'key' => 'options-page', 'value' => array( 'unknown', ), ),
		'show_names' => true, // Show field names on the left
		'fields'     => array(
			array(
				'name' => __( 'Country flag', 'taxonomy-metadata' ),
				'desc' => __( 'flag for country', 'taxonomy-metadata' ),
				'id'   => 'ctry_flag', // no prefix needed since the options are one option array.
				'type' => 'file',
			),
		)
	);

	// (Recommended) Use wp-large-options
	require_once( 'wp-large-options/wp-large-options.php' );
	$overrides = array(
		'get_option'    => 'wlo_get_option',
		'update_option' => 'wlo_update_option',
		'delete_option' => 'wlo_delete_option',
	);

	/**
	 * Instantiate our taxonomy meta class
	 */
	$cats = new Taxonomy_MetaData_CMB( 'paises', $meta_box, __( 'Country Settings', 'taxonomy-metadata' ), $overrides );
}
cmb_taxonomy_meta_initiate();