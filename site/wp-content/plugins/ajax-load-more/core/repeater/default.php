<div class="col-sm-4 col-xs-12 noticia">
    <?php 
        $post_exclude[]=get_the_ID(); ?>
        <div class="image-news">
            <?php $image_auxiliar_news_1 = wp_get_attachment_image( get_post_meta( get_the_ID(), '_wpb_img_aux_id', 1 ), 'full' );
            echo $image_auxiliar_news_1; ?>
        </div>
        <h3><?php the_title(); ?></h3>
        <p><?php echo substr(get_the_excerpt(), 0,160); ?></p>
        <a href="<?php the_permalink() ?>">leer más</a>
</div>